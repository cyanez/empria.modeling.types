﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Empira.Modeling.Types {
  class Library {

    #region Constructors and Parser

    private Library(string sourceLibrary){     
      this.SourceFileName = sourceLibrary;  
    
    }

    public static Library Parse(string libraryFile){
      var library = new Library(libraryFile);
      return library;
    }

    #endregion Constructors and Parser

    #region Properties
   
    public string SourceFileName {
      get;
      private set;
    }

    public string Namespace {
      get { Assembly assembly = Assembly.LoadFrom(this.SourceFileName);
        return assembly.GetName().Name;
      }
    }

    #endregion

    #region Private Methods
       
      
    #endregion Private Methods

    #region Public Methods

    private List<Type> GetClasses() {
      Assembly assembly = Assembly.LoadFrom(this.SourceFileName);
      Type[] types = assembly.GetTypes();
      List<Type> classes = new List<Type>();
      foreach (Type type in types) {
        if (type.IsClass) {
          classes.Add(type);
        }
      }
      return classes;
    }

    public List<Type> GetClassesByNamespace(string namespaceName) {
      List<Type> classes = GetClasses();
      List<Type> classesByNamespace = new List<Type>();
      foreach (Type type in classes) {
        if (type.Namespace == namespaceName) {
          classesByNamespace.Add(type);
        }
      }
      return classesByNamespace;
    }

    public List<Type> GetClassesByBaseType(string baseType) {
      List<Type> classes = GetClasses();
      List<Type> classByBaseType = new List<Type>();
      foreach (Type type in classes) {
        if (type.BaseType.FullName == baseType) {
          classByBaseType.Add(type);
        }
      }
      return classByBaseType;
    }

    public void SaveClassPropertiesRelations() {
      List<Type> classes = GetClassesByNamespace(this.Namespace);
      foreach (Type type in classes) {
        var typeClass = TypeClass.Parse(type);
        typeClass.SavePropertiesRelation();
      }
    }
   
   
    #endregion Public Methods


  }
}
