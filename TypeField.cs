﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Empira.Modeling.Types {
  class TypeField {

    #region Constructors and parsers

    private TypeField (Type field){
      this.Field = field;
    }

    public static TypeField Parse(Type field) {
      var typeField = new TypeField(field);
      return typeField;
    }   

    #endregion Constructors and parsers

    #region Properties
    public Type Field{
      get;
      private set;
    }
    
    #endregion Properties

    #region Private Methods

    #endregion Private Methods

    #region Public Methods

    public string GetFieldValue(string fieldName) {
      FieldInfo field = this.Field.GetField(fieldName, BindingFlags.Public |
                                             BindingFlags.NonPublic |
                                             BindingFlags.Instance |
                                             BindingFlags.Static);
      if (field == null) {
        return string.Empty;
      } else {
        return field.GetValue(null).ToString();
      }
    }

    #endregion Public Methods

  }
}
