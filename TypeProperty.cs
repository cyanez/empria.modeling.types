﻿using System;
using System.Collections.Generic;
using System.Reflection;

using Empiria.Data;

namespace Empira.Modeling.Types {
  class TypeProperty {
    
    #region Constructors and parsers

    private TypeProperty (PropertyInfo property){
      this.Property = property;
      this.Name = property.Name;
      this.DefaultValue = string.Empty;
      this.IsSealed = false;
      this.IsRuleBased = false;
      this.IsHistorizable = false;
      this.IsKeyword  = false;
      this.ProtectionMode = 0;
      this.AttributeSize = 0;
      this.AttributePrecision = 0;
      this.AttributeScale = 0;
      this.AttributeMinValue = string.Empty;
      this.AttributeMaxValue = string.Empty;
      this.AttributeDisplayFormat = string.Empty;

    }

    public static TypeProperty Parse(PropertyInfo property) {
      var  typeProperty = new TypeProperty(property);
      return typeProperty;
    }   

    #endregion Constructors and parsers

    #region Properties

    public PropertyInfo Property{
      get;
      private set;
    }

    public string Name {
      get;
      private set;
    }
           
    public string PropertetyTypeName{
      get { return this.Property.PropertyType.Name; }
    }

    public int EmpiriaTypeId {
      get {return GetEmpiriaTypeId();}      
    }
        
    public string DefaultValue {
      get;
      private set;
    }  
    
    public bool IsBidirectional{
      get{if ((this.Property.CanRead) && (this.Property.CanWrite)) {
           return true;
         } else {
           return false;
         } //else
       }    //if
    }
    
    public bool IsSealed{
      get;
      private set;
    } 
   
    public bool IsRuleBased{
      get;
      private set;
    }

    public bool ISReadOnly{
      get{if ((this.Property.CanRead) && (!this.Property.CanWrite)) {
        return true;
      } else {
        return false;
      } //else
      } //if
    }

    public bool IsHistorizable{
      get;
      private set;
    }

    public bool IsKeyword{
      get;
      private set;
    }

    public int ProtectionMode{
      get;
      private set;
    }

    public int AttributeSize{
      get;
      private set;
    }

    public int AttributePrecision{
      get;
      private set;
    }

    public int AttributeScale{
      get;
      private set;
    }

    public string AttributeMinValue{
      get;
      private set;
    }

    public string AttributeMaxValue{
      get;
      private set;
    }

    public string AttributeDisplayFormat{
      get;
      private set;
    }

    #endregion Properties

    #region Private Methods

    private int GetEmpiriaTypeId() {
      string query = "SELECT * FROM EOSTypes WHERE TypeName = '" + this.PropertetyTypeName + "'";      
      var operation = DataOperation.Parse(query);
      if(DataReader.GetFieldValue(operation, "TypeId") == null){
        return -1;
      } else{
         return (int) DataReader.GetFieldValue(operation, "TypeId");      
      }
    }     
    
    #endregion Private Methods

    #region Public Methods
    
    public void SavePropertyRelation(int sourceTypeId, string className){
      int TypeRelationId = DataWriter.CreateId("EOSTypeRelations");
      var operation = DataOperation.Parse("writeEOSTypeRelation", TypeRelationId, sourceTypeId, this.EmpiriaTypeId, 22,
           "Attribute", this.Name, this.Name, this.Name, this.Name, this.Name,
          this.DefaultValue, this.IsBidirectional, this.IsSealed, this.IsRuleBased, this.ISReadOnly,this.IsHistorizable, this.IsKeyword, this.ProtectionMode,
          className, "SourceId", "TargetId", "TypeRelationId", "0  ",
         this.AttributeSize, this.AttributePrecision, this.AttributeScale, this.AttributeMinValue, this.AttributeMaxValue, this.AttributeDisplayFormat, 
         false, -3, Convert.ToDateTime("23-10-2007"), "A");
      DataWriter.Execute(operation);   

    }
   
    #endregion Public Methods

  }
}
