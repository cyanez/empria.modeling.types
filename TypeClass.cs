﻿using System;
using System.Collections.Generic;
using System.Reflection;

using Empiria.Data;


namespace Empira.Modeling.Types {
  class TypeClass {

    #region Constructors and parsers

    private TypeClass (Type sourceClassType){
      this.Class = sourceClassType;
      this.Name = this.Class.Name;
    }
    
    public static TypeClass  Parse(Type sourceClassType){
      var typeClass = new TypeClass(sourceClassType);
      return typeClass;
    }   

    #endregion Constructors and parsers

    #region Properties
    
    public Type Class{
      get;
      private set;
    }
                 
    public string Name{
      get;
      private set;
    }
    
    public string Namespace{
      get;
      private set;
    }

    public string BaseType{
      get;
      private set;
    }

    public int EmpiriaTypeId{
      get{ 
        return GetEmpiriaTypeId(); 
      } 
    }     
    
    public bool isPrviate{
      get {return this.Class.IsNotPublic;}      
    }

    public bool isAbstract{
      get { return this.isAbstract;}    
    }
    
    #endregion Properties

    #region Private Methods

    private string GetFieldByName(string fieldName){
      var typeField = TypeField.Parse(this.Class);
      string fieldValue = typeField.GetFieldValue(fieldName);  
      return fieldValue;
    }
    
    private int GetEmpiriaTypeId(){
      string query = "SELECT * FROM EOSTypes WHERE TypeName = '" + GetFieldByName("thisTypeName") + "'"; 
      var operation = DataOperation.Parse(query);
      object empiriaTypedId = DataReader.GetFieldValue(operation, "TypeId");
      if (empiriaTypedId == null) {
        return -1;
      } else{
        return (int) empiriaTypedId;
      }     
    }

   
 
    #endregion Private Methods

    #region Public Methods
    
    public void SavePropertiesRelation(){     
     PropertyInfo [] properties = Class.GetProperties(); 
      foreach(PropertyInfo property in properties){
        var empiriaProperty = TypeProperty.Parse(property);
        empiriaProperty.SavePropertyRelation(this.EmpiriaTypeId,this.Name);
      }
    }
  

    #endregion Public Methods

  }
}
