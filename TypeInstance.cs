﻿using System;
using System.Collections.Generic;
using System.Reflection;


namespace Empira.Modeling.Types {
  class TypeInstance {
    
    #region Constructors and parsers

    private TypeInstance(string soruceFileName) {
      this.SourceFileName = soruceFileName;
      this.AsesemblyName = Assembly.LoadFrom(this.SourceFileName); 
      this.Namespace = this.AsesemblyName.GetName().Name;
    }

    public static TypeInstance Parce(string sourceFileName) {
      var instance = new TypeInstance(sourceFileName);
      return instance;
    }   

    #endregion Constructors and parsers

    #region Properties

    public string SourceFileName{
      get;
      private set;
    }

    public Assembly AsesemblyName{
      get;
      private set;    
    }
    
    public string Namespace{
      get;
      private set;
    }

    #endregion Properties

    #region Private Methods

    private Type[] GetTypes(){
      return this.AsesemblyName.GetTypes();
    }

    private List<Type> GetClasses(){
      Type[] instanceTypes = GetTypes();
      List<Type> classList =new List<Type>();
      foreach(Type type in instanceTypes){     
        if (type.IsClass) {
          classList.Add(type);          
        }
      }
      return classList;
    }

    #endregion Private Methods

    #region Public Methdos
    public  List<Type> GetClassesByNamespace(string namespaceName){
      List<Type> classes = GetClasses();
      List<Type> classByNamespace = new List<Type>();
      foreach (Type className in classes) {
        if (className.Namespace == namespaceName) {
          classByNamespace.Add(className);
        }
      }
      return classByNamespace;   
    }

    public List<Type> GetClassesByBaseType(string baseType){
      List<Type> classes = GetClasses();
      List<Type> classByBaseType = new List<Type>();
      foreach (Type className in classes) {
        if (className.BaseType.FullName  == baseType) {
          classByBaseType.Add(className);
        }      
      }
      return classByBaseType;
    }

    public void SaveClassPropertiesRelations(){
      List<Type> classes = GetClassesByNamespace(this.Namespace);      
      foreach (Type currentClass in classes) {
        var typeClass = TypeClass.Parse(currentClass);
        typeClass.SavePropertyRelations();        
      }
    }

    #endregion Public Methods


  }
}
